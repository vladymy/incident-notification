import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './_modules/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SuccessComponent } from './success/success.component';
import { LoginComponent } from './login/login.component';
import { NotificatorComponent } from './notificator/notificator.component';
import { TimePickerComponent } from './notificator/time-picker/time-picker.component';
import { InstructionComponent } from './instruction/instruction.component';

import { AlertComponent } from './alert/alert.component';

@NgModule({
  declarations: [
    AppComponent,
    SuccessComponent,
    LoginComponent,
    NotificatorComponent,
    TimePickerComponent,
    InstructionComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    AppRoutingModule,
    HttpClientModule
  ],
  entryComponents: [TimePickerComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
