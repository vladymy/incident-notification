import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotificatorComponent } from './notificator/notificator.component';
import { LoginComponent } from './login/login.component';
import { SuccessComponent } from './success/success.component';
import { InstructionComponent } from './instruction/instruction.component';

const routes: Routes = [
    {
        path: '', redirectTo: 'login', pathMatch: 'full'
    }, {
        path: 'login',
        component: LoginComponent
    }, {
        path: 'instruction',
        component: InstructionComponent
    }, {
        path: 'incident',
        component: NotificatorComponent
    }, {
        path: 'success',
        component: SuccessComponent
    }, {
        path: '**', redirectTo: 'notificator', pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes,
            { enableTracing: true }
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
