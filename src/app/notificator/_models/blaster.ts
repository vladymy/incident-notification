export class Blaster {
    id = 0;
    name = '';
    llc = '';
    location = '';

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
