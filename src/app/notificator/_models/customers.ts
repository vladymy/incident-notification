export class Customers {
    id = 0;
    name = '';
    location = '';

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
