import { NotificatorComponent } from '../notificator.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { Component, OnInit, Inject } from '@angular/core';
import { Input } from '@angular/core/src/metadata/directives';
@Component({
  selector: 'app-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.sass']
})
export class TimePickerComponent implements OnInit {
  inputValues = [
    {
      name: 'Hours',
      value: '00',
      active: true
    },
    {
      name: 'Minutes',
      value: '00',
      active: false
    }
  ];
  timeMode = 'AM';

  constructor(
    public dialogRef: MatDialogRef<NotificatorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data) {
      this.inputValues = [data.hours, data.minutes];
      this.timeMode = data.timeMode;
      this.inputValues[0].active = true;
      this.inputValues[1].active = false;
    }
  }

  enterValue(enteredVal: number): void {
    this.inputValues.map((inputItem, index) => {
      if (inputItem.active && (enteredVal <= 10)) {
        const maxValue = (inputItem.name === 'Hours') ? 12 : 60;
        if (inputItem.value.charAt(0) !== '0') {
          inputItem.value = '0' + enteredVal;
        } else {
          inputItem.value = inputItem.value.charAt(1) + enteredVal;
          if ((parseInt(inputItem.value, 10) > maxValue)) {
            inputItem.value = '0' + enteredVal;
          }
        }
        if ((inputItem.name === 'Hours') &&
            ((inputItem.value.charAt(0) !== '0') || (parseInt(inputItem.value + '0', 10) > maxValue))) {
          inputItem.active = false;
          this.inputValues[index + 1].active = true;
          enteredVal = parseInt( this.inputValues[index + 1].value, 10 );
        }
      }
    });
  }
  toggleActiveInput($event): void {
    this.inputValues.map(inputItem => {
      inputItem.active = !inputItem.active;
    });
  }
  changeTimeMode(newTimeMode): void {
    this.timeMode = newTimeMode  ;
  }
  reset(): void {
    this.dialogRef.close();
  }
  clear(): void {
    this.inputValues = [
      {
        name: 'Hours',
        value: '00',
        active: true
      },
      {
        name: 'Minutes',
        value: '00',
        active: false
      }
    ];
    this.timeMode = 'AM';
  }
  save(): void {
    this.dialogRef.close({
      inputValues: this.inputValues,
      timeMode: this.timeMode
    });
  }

  ngOnInit() {
    console.log(this.data);
  }

}
