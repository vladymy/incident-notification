import { Component, OnInit } from '@angular/core';
import { Blaster } from './_models/blaster';
import { Customers } from './_models/customers';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material';
import * as $ from 'jquery';
import { TimePickerComponent } from './time-picker/time-picker.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-notificator',
  templateUrl: './notificator.component.html',
  styleUrls: ['./notificator.component.sass']
})
export class NotificatorComponent implements OnInit {
  user: any = {
    firstName: 'Neo',
    lastName: 'Anderston'
  };
  incidentTime: any = {
    hours: {
      name: 'Hours',
      value: '00',
      active: true
    },
    minutes: {
      name: 'Minutes',
      value: '00',
      active: false
    },
    timeMode: 'AM'
  };

  selectedBlaster = new Blaster();
  selectedBlasterLlc: any;
  selectedBlasterLocation: any;
  selectedCustomer = new Customers();
  selectedCustomerLocation: any;

  blastersData: any = [];
  blasterLlcData: any = [];
  blasterLocationData: any = [];
  customersData: any = [];
  customerLocationData: any = [];
  formSubmited = false;

  filteredBlasters = [];
  filteredCustomers = [];
  notificationForm: FormGroup;

  constructor(private http: HttpClient, public dialog: MatDialog, private formBuilder: FormBuilder, private router: Router) {
  }
  selectBlaster(blaster): void {
    this.selectedBlaster = blaster;
  }

  selectCustomer(customer): void {
    this.selectedCustomer = customer;
  }

  ngOnInit(): void {
    this.http.get('assets/dummyData/dummyBlasters.json').subscribe(data => {
      this.blastersData = data;
    });

    this.notificationForm = this.formBuilder.group({
      incidentType: '',
      incidentDateValue: new Date(),
      incidentTime: '',
      blasterName: ['', Validators.required],
      blasterLlc: ['', Validators.required],
      blasterLocation: ['', Validators.required],
      customerName: ['', Validators.required],
      customerLocation: ['', Validators.required],
      selectedInjurie: '',
      selectedDamage: ''
    });

    const blasterNameCtrl = this.notificationForm.get('blasterName');
    const blasterLlcCtrl = this.notificationForm.get('blasterLlc');
    const blasterLocationCtrl = this.notificationForm.get('blasterLocation');
    const customerNameCtrl = this.notificationForm.get('customerName');
    const customerLocation = this.notificationForm.get('customerLocation');

    blasterLlcCtrl.disable();
    blasterLocationCtrl.disable();
    customerNameCtrl.disable();
    customerLocation.disable();

    blasterNameCtrl.valueChanges.subscribe((blaster) => {
      const paramsToSend = (blaster.name) ? { id: blaster.id } : {};
      const blasterName = (blaster.name) ? blaster.name : blaster;
      if (blaster) {
        blasterNameCtrl.setValue(blasterName, {emitEvent: false});
        this.filteredBlasters = this.blastersData.filter(blasterItem => {
          return blasterItem.name.toLowerCase().indexOf(blasterName.toLowerCase()) === 0;
        });
        // create http request for llc
        this.http.get('assets/dummyData/dummyBlastersLlc.json', { params: paramsToSend }).subscribe(data => {
          this.blasterLlcData = data;
          blasterLlcCtrl.enable();
        });
      } else {
        blasterLlcCtrl.disable();
        this.filteredBlasters = this.blastersData.slice();
      }
    });
    blasterLlcCtrl.valueChanges.subscribe((blasterLlc) => {
      if (blasterLlc) {
        // create http request for blaster location
        this.http.get('assets/dummyData/dummyBlastersLocation.json', {
          params: {
            id: blasterLlc.id
          }
        }).subscribe(data => {
          this.blasterLocationData = data;
          blasterLocationCtrl.enable();
        });
      } else {
        blasterLocationCtrl.disable();
      }
    });
    blasterLocationCtrl.valueChanges.subscribe((blasterLocation) => {
      if (blasterLocation) {
        this.http.get('assets/dummyData/dummyCustomers.json', {
          params: {
            id: blasterLocation.id
          }
        }).subscribe(data => {
          this.customersData = data;
          customerNameCtrl.enable();
        });
      } else {
        customerNameCtrl.disable();
      }
    });
    customerNameCtrl.valueChanges.subscribe((customer) => {
      const paramsToSend = (customer.name) ? { id: customer.id } : {};
      const customerName = (customer.name) ? customer.name : customer;
      if (customer) {
        customerNameCtrl.setValue(customerName, {emitEvent: false});
        this.filteredCustomers = this.customersData.filter((customerItem) => {
          return customerItem.name.toLowerCase().indexOf(customerName.toLowerCase()) === 0;
        });
        this.http.get('assets/dummyData/dummyCustomersLocation.json', { params: paramsToSend }).subscribe(data => {
          this.customerLocationData = data;
        });
        customerLocation.enable();
      } else {
        customerLocation.disable();
        return this.customersData.slice();
      }
    });
    console.log('notificator-loaded');
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(TimePickerComponent, { data: this.incidentTime });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.incidentTime.hours = result.inputValues[0];
        this.incidentTime.minutes = result.inputValues[1];
        this.incidentTime.timeMode = result.timeMode;
        this.notificationForm.get('incidentTime').setValue(this.incidentTime.hours.value + ' : ' +
          this.incidentTime.minutes.value + ' ' + this.incidentTime.timeMode);
      }
    });
  }

  onSubmit(): void {
    this.formSubmited = true;
    const submitData = {
      blaster: this.selectedBlaster,
      blasterLlc: this.selectedBlasterLlc,
      blasterLocation: this.selectedBlasterLocation,
      customer: this.selectedCustomer,
      customerLocation: this.selectedCustomerLocation
    };
    if (this.notificationForm.invalid) {
      const el = $('.ng-invalid:not(form):first');
      $('html,body').animate({ scrollTop: (el.offset().top - 20) }, 'slow', () => {
        el.focus();
      });
    } else {
      this.router.navigateByUrl("success");
    }

    // console.log(submitData);
    // this.http.post('some/apipoint', submitData).subscribe(
    //   data => {
    //     console.log(data);
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // );
  }
}
