import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginError = false;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  login(): void {
    if (this.loginError) {
      this.router.navigateByUrl('instruction');
    }
    this.loginError = !this.loginError;
  }

}
